## to create a new branch and switch to it now ...
git checkout -b hw01

## to commit the new branch ...
```
git commit -m "branch hw02 created"
```

## to push the changes ...
```shell
git push
```

.......

On branch hw02
nothing to commit, working tree clean
[demo@almalinux rudisali]$ git push
fatal: The current branch hw02 has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin hw02 <<<<<< -----

To have this happen automatically for branches without a tracking
upstream, see 'push.autoSetupRemote' in 'git help config'.

......


git push --set-upstream origin hw02

......

Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
remote: 
remote: To create a merge request for hw02, visit:
remote:   https://gitlab.com/krudisar/rudisali/-/merge_requests/new?merge_request%5Bsource_branch%5D=hw02
remote: 
To gitlab.com:krudisar/rudisali.git
 * [new branch]      hw02 -> hw02
branch 'hw02' set up to track 'origin/hw02'.

......


=========
GIT MERGE
=========



# step #1 -> switch to main branch 
# ================================
git checkout main





# step #2 -> merge changes from branch hw02 -> main
# =================================================
git merge hw02


Updating c4a5b12..709c332
Fast-forward
 hw02/hw02.c | 3 +++
 1 file changed, 3 insertions(+)
 create mode 100644 hw02/hw02.c




# step #3 -> check the status
# ===========================

git status
On branch main
Your branch is ahead of 'origin/main' by 2 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean




# step #4 -> push the changes to remote .... and # check GitLab UI 
# ================================================================
git push



# step #5 -> check if branch still exists
git branch

 hw02
* main


# step #6 -> delete the branch locally

git branch -d hw02

Deleted branch hw02 (was 709c332).

# now is deleted locally, but it still exists on remote (GitLab)
# to get a list of branches on the remote

git branch -a

* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/hw02
  remotes/origin/main



# to delete the branch on remote

git push origin -d hw02

To gitlab.com:krudisar/rudisali.git
 - [deleted]         hw02

# check if the branch is really deleted from remote (GitLab)

git branch -a

* main
  remotes/origin/HEAD -> origin/main
  remotes/origin/main

# .. or check GitLab UI to validate that hw02 branch is gone ...















