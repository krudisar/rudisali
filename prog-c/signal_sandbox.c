//
// ascii table -> https://www.rapidtables.com/code/text/ascii-table.html
//

#include <stdio.h>
#include <math.h>
#include <string.h>

int getSum(char part[]) {
    int totalSum = 0;
    char a_char = 97;
    
    for (int i=0; i < strlen(part); i++) {
        // totalSum = totalSum + round(pow(2, part[i] - a_char));
        // totalSum += round(pow(2, part[i] - a_char));
    }
    return totalSum;
}

int main() {
    char a = 97;
    printf("a=%d\n", a);
    printf("a=%c\n", a);
    printf("a=%c\n", ++a);
    printf("a=%c\n", ++a);
    
    char ch1 = 'a';
    char ch2 = 'e';
    
    printf("difference=%d\n", ch2 - ch1);
    printf("decoded value for '%c' is %i\n", ch2, (int) round(pow(2, ch2 - ch1)));

    char test[] = "aabbccddee";
    printf("\n");
    printf("decoded value for '%s' is %i\n", test, getSum(test));
    return 0;
}