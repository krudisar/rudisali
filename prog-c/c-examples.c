
#include <stdio.h>
int main() {     
   
    int mark[5] = {19, 10, 8, 17, 9};

    size_t size1 = sizeof(mark);
    printf("Size of the whole array is ... %zu bytes\n", size1);
    size1 = sizeof(mark[0]);
    printf("Size of the single item in the array is ... %zu bytes\n", size1);

    //
    printf("%d\n",mark[0]);
    printf("%p\n", &mark[0]);
    printf("%p -> %d\n", (void *) (size_t) mark[0], (int) (mark[0]));    

    printf("%d\n",mark[1]);
    printf("%p\n", &mark[1]);
    printf("%p -> %d\n", (void *) (size_t) mark[0], (int) (mark[0])); 

    char s[5] = {'A', 'B', 'C', 'D', '\0'};

    size_t size2 = sizeof(s);
    printf("Size of the whole array is ... %zu bytes\n", size2);
    size2 = sizeof(s[0]);
    printf("Size of the single item in the array is ... %zu bytes\n", size2);

    //

    printf("%s\n",s);
    printf("%p\n",(void *) &s);

    printf("\n");
    printf(" - %c\n",s[0]);
    printf(" - %p\n",(void *) &s[0]);
    printf(" - %p -> %c\n", s[0], s[0]);    

    printf("\n");
    printf(" - %c\n",s[1]);
    printf(" - %p\n",(void *) &s[1]);
    printf(" - %p -> %c\n", s[1], s[1]); 

    //
    char *p;

    printf("\n... A ted pointer na 'char'\n\n");

    p = s; // or p = &s[0];

    printf("%p\n", p);
    printf("%p\n", ++p);    
    printf("%p\n", ++p);

    //
    int *pi;

    printf("\n... A ted pointer na 'int'\n\n");    
    pi = mark;
    printf("%p\n", pi);
    printf("%p\n", ++pi);    
    printf("%p\n", ++pi);

    return 0;
}