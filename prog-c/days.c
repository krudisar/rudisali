#include <stdio.h> 
#include <string.h> 
#include <assert.h>

// lze nadefinovat macro pro pripady, kdy se v kodu opakuji nektere stejne sekce (radky) 
// nekolikrat -> zprehledneni kodu ...

#define INCREASE_DAYS() {\
    totalDays++;\
    if ( getWeekDay(d, m, y) >=1 && getWeekDay(d, m, y) <= 5 && checkHoliday(d,m) == 0){\
        totalWorkDays++;\
    }\
    };

// pocty dni v mesici pro neprechodny a prechodny rok
const int dim[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const int dim_leap[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

// vrati cislo 0-6 pro jednotlive dny v tydnu (0=Nedele, 1=Pondeli, ..., 6=Sobota)
// source: https://gist.github.com/bharathyes/b9b0f90c71ddc78d02a7
// source: https://stackoverflow.com/questions/6054016/c-program-to-find-day-of-week-given-date
int getWeekDay(int d, int m, int y){
    return (d += m < 3 ? y-- : y - 2, 23*m/9 + d + 4 + y/4- y/100 + y/400)%7;  
}

// test, zda je zadany rok prechodny (ret=0) nebo ne (ret=1)
// source: https://www.programiz.com/c-programming/examples/leap-year
int checkLeapYear(int year){

    // leap year if perfectly divisible by 400
    if (year % 400 == 0) {
        return 0;
    }
    // not a leap year if divisible by 100
    // but not divisible by 400
    else if (year % 100 == 0) {
        return 1;
    }
    // leap year if not divisible by 100
    // but divisible by 4
    else if (year % 4 == 0) {
        return 0;
    }
    // all other years are not leap years
    else {
        return 1;
    }
}

// kontrola, zda je zadany datum (d,m) svatni svatek
int checkHoliday(int d,int m){
    const char *svatky[] = {"1.1","1.5","8.5","5.7","6.7","28.9","28.10","17.11","24.12","25.12","26.12"};
    int pocet_svatku = sizeof(svatky) / sizeof(svatky[0]);

    // vytvorime string ve tvaru 'd.m' ...
    char tmp1[6];
    char tmp2[6];

    //
    //
    sprintf(tmp1, "%d", d);
    strcat(tmp1, ".");
    sprintf(tmp2, "%d", m);
    strcat(tmp1, tmp2);
    //
    //

    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //sprintf(tmp1, "%d.%d", d, m);
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // ... a otestujeme, zda se nachazi v poli svatky[]
    for (int i=0; i<pocet_svatku; i++){
        if (strcmp(svatky[i], tmp1) == 0){
            //printf("datum %s je svatek\n", svatky[i]);
            return 1;
        }
    }
    return 0;
}

// vystupni struktura ve tvaru dle zadani
struct retStruct{
    int m_TotalDays;
    int m_WorkDays;
};

struct retStruct countDays(int y1, int m1, int d1, int y2, int m2, int d2){

    struct retStruct result = {0};

    int d = 0;
    int m = 0;
    int y = 0;
    
    int totalDays = 0;
    int totalWorkDays = 0;

    int dm = 0;

    //
    // TEST NA SPATNE VSTUPNI HODNOTY
    //
    
    // CHYBA: cilovy rok je mensi nez ten prvni
    if (y2 < y1){
        result.m_TotalDays = -1;
        result.m_WorkDays = -1;
        return result; 
    }
    // CHYBA: den v unoru je vetsi nez 28, kdyz je ten rok neprechodny
    if ( m1 == 2 && d1>28 && (checkLeapYear(y1) != 0) ){
        result.m_TotalDays = -1;
        result.m_WorkDays = -1;
        return result; 
    }
    // CHYBA: den v unoru je vetsi nez 28, kdyz je ten rok neprechodny
    if ( m2 == 2 && d2>28 && (checkLeapYear(y2) != 0) ){
        result.m_TotalDays = -1;
        result.m_WorkDays = -1;
        return result; 
    }

    //
    // DOBRE VSTUPNI HODNOTY
    //
    if (y1 == y2){

        if (m1==m2){
            // stejny mesic? stejny den?
            if (d1==d2){
                totalDays=1;
                if ( getWeekDay(d1, m1, y1) >=1 && getWeekDay(d1, m1, y1) <= 5  && checkHoliday(d1,m1) == 0) {
                    totalWorkDays++;
                } 
            }
            // stejny mesic? dopocitej pouze dny
            else{
                for (int i=d1;i<=d2;i++){
                    totalDays++;
                    if ( getWeekDay(i, m1, y1) >=1 && getWeekDay(i, m1, y1) <= 5  && checkHoliday(i,m1) == 0) {
                        totalWorkDays++;
                    }      
                } 
            }
            // koncime
            result.m_TotalDays = totalDays;
            result.m_WorkDays = totalWorkDays;                                                 
            return result; 
        }

        y = y1;

        for (m=m1; m<=m2; m++){

            if (checkLeapYear(y) == 0){
                dm = dim_leap[m-1];
            }
            else{
                dm = dim[m-1];
            }        

            for (d=1; d<=dm; d++){

                // dny od pocatecniho data do konce mesice (m1)   
                if (d >= d1 && m==m1){
                    //printf("%d.%d.%d\n", d,m,y);
                    totalDays++;
                    if ( getWeekDay(d, m, y) >=1 && getWeekDay(d, m, y) <= 5  && checkHoliday(d,m) == 0){
                        totalWorkDays++;
                    }
                    continue;
                }

                // dny v celych mesicich (m1+1 -> m2-1)
                if ( (m > m1) && (m < m2) ) {
                    //printf("%d.%d.%d\n", d,m,y);
                    totalDays++;
                    if ( getWeekDay(d, m, y) >=1 && getWeekDay(d, m, y) <= 5  && checkHoliday(d,m) == 0){
                        totalWorkDays++;
                    }
                    continue;
                }
               
                // dny do ciloveho data v poslednim mesici (m2)
                if (d <= d2 && m==m2){
                    //printf("%d.%d.%d\n", d,m,y);
                    totalDays++;
                    if ( getWeekDay(d, m, y) >=1 && getWeekDay(d, m, y) <= 5  && checkHoliday(d,m) == 0){
                        totalWorkDays++;
                    }
                    continue;
                }

                // jsme na konci u druheho datumu?    
                if (d == d2 && m == m2) {
                    break;
                }
            }
        }
    } // if
    else{
        for (y=y1; y<=y2; y++){

            for (m=1;m<=12;m++){

                if (checkLeapYear(y) == 0){
                    dm = dim_leap[m-1];
                }
                else{
                    dm = dim[m-1];
                }

                for (d=1;d<=dm;d++){

                    if (d >= d1 && m==m1 && y==y1){
                        INCREASE_DAYS()
                        continue;
                    }

                    if (d <= d2 && m==m2 && y==y2){
                        INCREASE_DAYS()
                        continue;
                    }
                    
                    if (m > m1 && y == y1){
                        INCREASE_DAYS()
                        continue;
                    }

                    if (m < m2 && y == y2){
                        INCREASE_DAYS()
                        continue;
                    }

                    if (y>y1 && y<y2){
                        INCREASE_DAYS()
                    }
                }
            }
        }
    } // else

    // koncime
    result.m_TotalDays = totalDays;
    result.m_WorkDays = totalWorkDays;
    return result; 
}

int main(){
    struct retStruct r;

  r = countDays ( 2023, 11,  1,
                  2023, 11, 30 );
  assert ( r . m_TotalDays == 30 );
  assert ( r . m_WorkDays == 21 );

  r = countDays ( 2023, 11,  1,
                  2023, 11, 17 );
  assert ( r . m_TotalDays == 17 );
  assert ( r . m_WorkDays == 12 );

  r = countDays ( 2023, 11,  1,
                  2023, 11,  1 );

  assert ( r . m_TotalDays == 1 );
  assert ( r . m_WorkDays == 1 );

  r = countDays ( 2023, 11, 17,
                  2023, 11, 17 );
            
  assert ( r . m_TotalDays == 1 );
  assert ( r . m_WorkDays == 0 );

  r = countDays ( 2023,  1,  1,
                  2023, 12, 31 );
              
  assert ( r . m_TotalDays == 365 );
  assert ( r . m_WorkDays == 252 );

  r = countDays ( 2024,  1,  1,
                  2024, 12, 31 );

  assert ( r . m_TotalDays == 366 );
  assert ( r . m_WorkDays == 254 );

  r = countDays ( 2000,  1,  1,
                  2023, 12, 31 );

  assert ( r . m_TotalDays == 8766 );
  assert ( r . m_WorkDays == 6072 );

  r = countDays ( 2001,  2,  3,
                  2023,  7, 18 );
  assert ( r . m_TotalDays == 8201 );
  assert ( r . m_WorkDays == 5682 );


  r = countDays ( 2021,  3, 31,
                  2023, 11, 12 );
  assert ( r . m_TotalDays == 957 );
  assert ( r . m_WorkDays == 666 );

  r = countDays ( 2001,  1,  1,
                  2000,  1,  1 );

  assert ( r . m_TotalDays == -1 );
  assert ( r . m_WorkDays == -1 );


  r = countDays ( 2001,  1,  1,
                  2023,  2, 29 );

  assert ( r . m_TotalDays == -1 );
  assert ( r . m_WorkDays == -1 );            

  return 0;
}
//     printf("___ m_TotalDays: %d\t m_WorkDays:%d", r.m_TotalDays, r.m_WorkDays);