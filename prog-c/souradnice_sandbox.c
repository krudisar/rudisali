#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int main(){

    char souradnice[30] = "";
    int delka = 0;

    //printf("Enter name:\n");
    //fgets(souradnice, sizeof(souradnice), stdin);
    
    strcpy(souradnice, "(1.4545,2.1488)");
    delka = strlen(souradnice);
    //printf("Name: %c %c\n", souradnice[0], souradnice[delka - 1]);
    //printf("Delka: %d\n", delka);
    
    
    if (souradnice[0] == '(' && souradnice[delka - 1] == ')'){

        printf("Vypada to dobre!\n");
    }


    // parse string between brackets using 'strncpy'
    //
    char cisla[30];
    //
    strncpy(cisla, souradnice+1, delka-2);
    printf("Co je uvnitr zavorky: %s\n", cisla);


    // use split to get x & y part of the string using 'strtok'
    // info about 'strtok' -> https://www.codingame.com/playgrounds/14213/how-to-play-with-strings-in-c/string-split
    //
    const char delimiter[2] = ",";
    char *x_part;
    char *y_part;
    //    
    x_part = strtok(cisla, delimiter);
    y_part = strtok(NULL, delimiter);    
    printf("X: %s, Y: %s\n", x_part, y_part);


    char povolene_znaky[] = "1234567890.";
    int iFlag;
    
    for (int i = 0; i < strlen(x_part); i++) 
    {
        iFlag = 1;
        if (strchr(povolene_znaky, x_part[i]) != NULL)
        {
            printf("%c\n", x_part[i]);
        }
        else{
            iFlag = 0;
            break;
        }
    }
    printf("iFlag: %i\n", iFlag);
    
    printf("-------\n");

    iFlag = 1;    
    for (int i = 0; i < strlen(y_part); i++) 
    {
        if (strchr(povolene_znaky, y_part[i]) != NULL)
        {
            printf("%c\n", y_part[i]);
        }
        else{
            iFlag = 0;
            break;
        }
    }
    printf("iFlag: %i\n", iFlag);    
        
    // convert x & y part to double using 'atof'
    //
    double x_value, y_value = 0;    
    //
    x_value = atof(x_part);
    y_value = atof(y_part);
    printf("X: %f, Y: %f", x_value, y_value);
    
    return 0;
}
