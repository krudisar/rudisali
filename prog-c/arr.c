#include <stdio.h>
#include <stdint.h>

struct vysledek{
    int hodnota;
    int start;
    int stop;

};

int count = 1;
struct vysledek pole[10];

int main() {

    //int my_array[] = {23, 112, 5, 11, 8, 4, 16, -8, 3, 0, 1, 3};
    int my_array[] = {24, 25, 16, 18, 2, 4};

    int array_length = 0;
    array_length = sizeof(my_array) / sizeof(my_array[0]);

    int how_long = 0;
    int longest = 1;

    int m = 0;

    int start = 0;
    int stop = 0;
    int i;
    int a;

    for(i = 0; i < (array_length - 1); i++) {

        how_long = 0;
        for (a = i + 1; a < array_length; a++) {

            if (my_array[i] < my_array[a]){
                how_long = a - i + 1;

                if (how_long >= longest){
                    start = i;
                    stop = a;
                }
            }
        } // end of for

        if (how_long > longest){
            longest = how_long;
            count = 1;
            pole[count-1].start = start;
            pole[count-1].stop = stop;
            pole[count-1].hodnota = longest;
        }
        else if (how_long == longest) {
            count++;
            pole[count-1].start = start;
            pole[count-1].stop = stop;
            pole[count-1].hodnota = longest;
        }

    }

    // vytiskni vysledky
    for (int a=0; a<count;a++){
        printf("%d: %d - %d\n", pole[a].hodnota, pole[a].start, pole[a].stop);        
    }

}
