#include <stdio.h>
#include <string.h>

struct listStruct {
    int size;
    char name[10];
};

int main(){

    //
    // declare a few arrays of the type struct listStruct
    //
    struct listStruct c1[2] = {{1, "name1"},{11, "name2"}};
    struct listStruct c2[2] = {{.size=2, .name="name1"},{.size=22, .name="name2"}};
    struct listStruct c3[1] = {{.size=3, .name="name3"}};

    // GOAL: print the size of the array c1
    // expected is 10 bytes (name) and 4 bytes (size) => 28 bytes in total (array of 2)
    // ??? in reality the size is 32 bytes in total for array c1 -> WHY ???
    //
    // ... because C is doing some memory alignment 
    // https://www.geeksforgeeks.org/is-sizeof-for-a-struct-equal-to-the-sum-of-sizeof-of-each-member/
    //
    // !!! ALWAYS use sizeof(c1) to get real size of the array of struct or a single struct variable !!!
    //

    // print the size of the array c1
    printf("\nArray size in bytes: %lu\n", sizeof(c1));
    // print the pointer to array c1
    printf("A pointer to the array -> %p\n\n", c1);       // or printf("%p\n\n", &c1[0]);

    //
    // declare a pointer to 'struct listStruct'
    // print the pointer and do several ++ to see how the memory pointer is incremented
    struct listStruct *p;
    p = c2; // or -> p = &c2[0];
    printf("%p\n", p);
    printf("%p\n", ++p);
    printf("%p\n", ++p);

    return 0;
}
