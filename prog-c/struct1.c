#include <stdio.h>
#include <string.h>

// declare a struct
struct listStruct {
    int size;
    char name[10];
};

//
// function expects input parameter of type 'struct listStruct'
//
int printStruct(struct listStruct s){
    printf("\tSize: %d\tName: %s\n", s.size, s.name);
    return 0;
}

//
// function that returns value of type 'struct listStruct'
//
struct listStruct fce(){
    struct listStruct x; 
    x.size = 20;
    strcpy(x.name, "Karel Fce");
    return x;
}

//
// main()
//
int main(){

    struct listStruct a = { .size = 10, .name = "Karel"};

    printf("A pointer to the variable a -> %p\n\n", &a);
    printf("\t%s\n", a.name);

    //
    // call function expects input parameter of type 'struct listStruct'
    //
    printStruct(a);
    //

    //
    // call function that returns value of type 'struct listStruct'
    //
    struct listStruct b;
    b = fce();
    //
    printStruct(b);

    return 0;
}
