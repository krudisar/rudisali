#include <stdio.h> 
#include <stdlib.h> 


const int TRIANG_NONE = 0;        // binary 0000 0000
const int TRIANG_ACUTE = 1;       // binary 0000 0001
const int TRIANG_RIGHT = 2;       // binary 0000 0010
const int TRIANG_OBTUSE = 4;      // binary 0000 0100
const int TRIANG_DUPLICATES = 8;  // binary 0000 1000
const int TRIANG_ALL = TRIANG_ACUTE | TRIANG_RIGHT | TRIANG_OBTUSE;     // binary 0000 0111


int myfunction(int lo, int hi, int filter){
    int result = 0;
    int duplicates = 0;     // 0 = bez duplicit, 1 = vcetne duplicit
    int acute = 0;
    int right = 0;
    int obtuse = 0;

    int number_of_possible_triangles = 0;
    int num_1s = 0;
    int num_2s = 0;

    int num_1sa = 0;
    int num_2sa = 0;

    int total_acute = 0;
    int total_right = 0;
    int total_obtuse = 0;

    if (lo <= 0 || hi <= 0 || filter < 0) {
        return -1;
    }
    // check if filter is TRIANG_DUPLICATES
    if (filter == 8){
        // DUPLICATES ONLY is SET  -> do nothing and return 0
        return 0;
    }

    // check if filter is TRIANG_NONE only
    if (filter == 0){
        // NONE ONLY is SET -> do nothing and return 0
        return -1;
    }

    //checking 4th bit -> TRIANG_DUPLICATES - 0000 1000
    if(filter & (1<<3)){
        duplicates = 1;
    }

    for (int u = lo; u <= hi; u++) {
        for (int h = lo; h <= hi; h++) {
            for (int g = lo; g <= hi; g++) {
                if ((u + h > g) && (u + g > h) && (g + h > u)) {
                    number_of_possible_triangles += 1;
                    if (u == h && h == g && g == u) {
                        num_1s++;
                    } else {
                        if (u == h || h == g || g == u) {
                            num_2s++;
                        }
                    }

                    // vytvoreni a srovnani pole ---
                    int pole[3];
                    pole[0] = u;
                    pole[1] = h;
                    pole[2] = g;

                    int tmp = 0;
                    int n = 3;
                    for (int i = 0; i < n; ++i) {
                        for (int j = i + 1; j < n; ++j) {
                            if (pole[i] > pole[j]) {
                                tmp = pole[i];
                                pole[i] = pole[j];
                                pole[j] = tmp;
                            }
                        }
                    }
                    // ------------------------------

                    // ACUTE
                    if (pole[0] * pole[0] + pole[1] * pole[1] > pole[2] * pole[2]) {
                        acute += 1;
                        if (u == h && h == g && g == u) {
                            num_1sa++;
                        } else {
                            if (u == h || h == g || g == u) {
                                num_2sa++;
                            }
                        }
                    }
                    // RIGHT
                    // ....

                    // OBTUSE
                    // ...
                }
            }
        }    
    }


    //checking 1st bit
    if(filter & (1<<0)){

        // ACUTE is SET
        if (duplicates !=0){
            // spocitej vcetne duplicit                    
            total_acute = acute;
        }else{
            // spocitej bez duplicit
            total_acute = ((acute - num_1sa - num_2sa) / 6) + num_1sa + (num_2sa / 3);
        }

    }

/*
    //checking 2nd bit -> TRIANG_RIGHT
    if(filter & (1<<1)){
        // RIGHT is SET
        if (duplicates !=0){
            // spocitej vcetne duplicit                    
            total_right = 200;
        }else{
            // spocitej bez duplicit
            total_right = 100;
        }
    }

    //checking 3rd bit -> TRIANG_OBTUSE
    if(filter & (1<<2)){
        // OBTUSE is SET
        if (duplicates !=0){
            // spocitej vcetne duplicit                    
            total_obtuse = 2000;
        }else{
            // spocitej bez duplicit
            total_obtuse = 1000;
        }
    }
*/
    // celkovy pocet je soucet
    result = total_acute + total_right + total_obtuse;

    return result;
}
  
int main() 
{
    int retvalue;

    retvalue = myfunction(1,3,TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_NONE);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(10,50,TRIANG_ACUTE);
    printf("Vysledek: %d\n", retvalue);   

    retvalue = myfunction(10,50,TRIANG_ACUTE | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(31,39,TRIANG_ACUTE);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(31,39,TRIANG_ACUTE | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    


    retvalue = myfunction(42,42,TRIANG_ACUTE);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(42,42,TRIANG_ACUTE);
    printf("Vysledek: %d\n", retvalue);   

    retvalue = myfunction(0,14,TRIANG_NONE);
    printf("Vysledek: %d\n", retvalue);   



    /*
    retvalue = myfunction(1,3,TRIANG_ACUTE | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);   

    retvalue = myfunction(1,3,TRIANG_RIGHT);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_RIGHT | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_RIGHT | TRIANG_OBTUSE);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_RIGHT | TRIANG_OBTUSE | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_ACUTE | TRIANG_RIGHT | TRIANG_OBTUSE);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_ACUTE | TRIANG_RIGHT | TRIANG_OBTUSE | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3,TRIANG_ALL);
    printf("Vysledek: %d\n", retvalue);    

    retvalue = myfunction(1,3, 15 | TRIANG_DUPLICATES);
    printf("Vysledek: %d\n", retvalue);    
    */

    return 0; 
}

