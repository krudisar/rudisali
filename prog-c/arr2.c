#include <stdio.h>
#include <stdlib.h>
#define MAX 250000

//int ii = 0;


int main() {
    typedef struct structure {

        int length;
        int index1;
        int index2;
    } structure;

    printf("Hodnoty:\n");

    int my_array[MAX] = {0};
    int input = 0;
    int elements = 0;
    int f = 0;

    for (int b = 0; b < 250000; b++) {
        if (scanf("%d", &input) == 1) {
            my_array[f] = input;
            //printf("%d\n", f);
            f++;
            elements++;
        }
        else if(scanf("%d", &input) == EOF){
            break;
        }
        else if (scanf("%d", &input) != 1) {
            printf("Nespravny vstup.\n");
            exit(0);
        }

    }

        //int my_array[MAX] = {5, 9, 11, 8, 4, 16, -8, 3, 1, 3};


        int longest = 1;
        int start = 0;
        int stop = 0;
        structure result_array[MAX] = {0};
        int i;
        int a;
        int count = 0;


        for (i = 0; i < (elements - 1); i++) {

            int how_long = 0;

            for (a = i + 1; a < elements; a++) {

                if (my_array[i] < my_array[a]) {
                    how_long = a - i + 1;

                    if (how_long >= longest) {
                        start = i;
                        stop = a;
                    }

                }
            } // end of for


            if (how_long > longest) {
                longest = how_long;

                count = 1;
                result_array[count - 1].length = longest;
                result_array[count - 1].index1 = start;
                result_array[count - 1].index2 = stop;

            } else if (how_long == longest) {
                count++;
                result_array[count - 1].length = longest;
                result_array[count - 1].index1 = start;
                result_array[count - 1].index2 = stop;
            }

        }
        // vytiskni vysledky
        if ( count > 0) {
            for (int p = 0; p < count; p++) {
                printf("%d: %d - %d\n", result_array[p].length, result_array[p].index1, result_array[p].index2);
            }
            printf("Moznosti: %d\n", count);

        }

        if (count == 0) {

            printf("Nelze najit.\n");
        }

    }