#include <stdio.h> 
#include <string.h>

// Function to remove all spaces from a given string
void removeSpaces(char *str)
{
    // To keep track of non-space character count
    int count = 0;

    // Traverse the provided string. If the current character is not a space,
    // move it to index 'count++'.
    // 
    // 'for' loop is finished when condition (str[i] in this case is evaluated as False -> '\0' -> last char in the string)

    // char str[] = "GeeksforGeeks";
    // char str[] = { 'G','e','e','k','s','f','o','r','G','e','e','k','s','\0'};

    for (int i = 0; str[i]; i++)
        if (str[i] != ' ')
            str[count++] = str[i]; // here count is incremented
    str[count] = '\0';
}
// Driver program to test above function
int main()
{
    char str[] = "   ( 6, 333.  4 ) ";
    printf("Delka stringu -> %i\n", strlen(str));

    //removeSpaces(str);
    //printf("%s", str);
    
    for (int i = 0; i <= strlen(str); i++){

        if (str[i] == '\0') {
            printf("\n ... asi jsme narazili na konec ... i=%i", i);
        }
        else{
            printf("%c", str[i]);
        }
    }
    return 0;
}
